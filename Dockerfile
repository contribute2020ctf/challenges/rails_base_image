# Stolen^WInspired by https://thoughtbot.com/blog/rails-on-docker
FROM ruby:2.6-alpine

RUN apk add --no-cache \
build-base curl-dev sqlite-dev \
yaml-dev zlib-dev nodejs yarn tzdata
#RUN apt-get update -qq && apt-get install -y build-essential
ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
RUN gem install bundler -v 2.0.2
ADD Gemfile* $APP_HOME/
RUN bundle install --no-cache 

